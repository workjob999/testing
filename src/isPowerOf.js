const { performance } = require('perf_hooks')

const calPower = (num) => {
  let power = 2
  let start = 2
  let max = 2
  let result = []
  let nextMax = max

  while (max <= num) {
    power++
    max = Math.pow(start, power)
  }

  if (max > num) {
    power--
    max = Math.pow(start, power)
  }
  result = [max, start, power]

  while (power > 1) {
    start++
    nextMax = Math.pow(start, power)
    while (nextMax > num) {
      power--
      nextMax = Math.pow(start, power)
    }

    if (nextMax >= max) {
      max = nextMax
      result = [max, start, power]
    }
  }

  return result
}

const isPowerOf = (input) => {
  const t0 = performance.now()

  let result = []
  if (input === 1) {
    result = [0, -1]
  } else if (input < 4) {
    result = [1, -1]
  } else {
    result = calPower(input)
  }

  const t1 = performance.now()

  console.log(result)

  const diffTime = t1 - t0

  console.log(`time - ${diffTime} seconds`)
}

module.exports = {
  isPowerOf
}
