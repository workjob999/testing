const { port } = require('./config/app')
const { HEADERS } = require('./config/allow-headers')

const isPower = require('./isPowerOf')

const ExpressApp = require('../infrastructure/express-app')
const ExpressServer = require('../infrastructure/express-server')

const expressApp = new ExpressApp()
expressApp.init()
expressApp.allowHeaders(HEADERS.toString())

const expressServer = new ExpressServer(expressApp.getApp(), { port })
expressServer.start()

isPower.isPowerOf(1000002)

module.exports = expressServer.server
