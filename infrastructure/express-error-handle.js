const path = require('path')

const HTTP_MESSAGE = require('../src/config/http-message')

module.exports.handleURLNotFound = (req, res) => {
  res.status(HTTP_MESSAGE.CLIENT_ERROR.NOT_FOUND.CODE)
    .json({ message: HTTP_MESSAGE.CLIENT_ERROR.NOT_FOUND.MESSAGE })
}

module.exports.handleError = (err, req, res) => {
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  res.status(err.status || 500)
  res.sendFile(path.join(__dirname, '/views/error.html'))
}

module.exports.handleErrorJson = (err, req, res, next) => {
  if (err instanceof SyntaxError) {
    res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SYNTAX_ERROR.CODE,
      status: HTTP_MESSAGE.SYNTAX_ERROR.STATUS,
      errorStatus: HTTP_MESSAGE.SYNTAX_ERROR.ERROR_STATUS,
      errorMessage: HTTP_MESSAGE.SYNTAX_ERROR.MESSAGE
    })
  } else {
    next()
  }
}
